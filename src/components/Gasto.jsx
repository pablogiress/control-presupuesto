import React from 'react'
import {
    LeadingActions,
    SwipeableList,
    SwipeableListItem,
    SwipeAction,
    TrailingActions
} from 'react-swipeable-list'
import "react-swipeable-list/dist/styles.css"
import { generarFecha } from '../helpers'

import iconoAhorro from '../img/icono_ahorro.svg'
import iconoCasa from '../img/icono_casa.svg'
import iconoComida from '../img/icono_comida.svg'
import iconoGastos from '../img/icono_gastos.svg'
import iconoOcio from '../img/icono_ocio.svg'
import iconoSalud from '../img/icono_salud.svg'
import iconoSuscripciones from '../img/icono_suscripciones.svg'

const diccionarioIconos = {
    ahorro: iconoAhorro,
    comida: iconoComida,
    casa: iconoCasa,
    varios: iconoGastos,
    ocio: iconoOcio,
    salud: iconoSalud,
    suscripciones: iconoSuscripciones
}


const Gasto = ({gasto, setGastoEditar, eliminarGasto}) => {

    const editarAction = () => (
        <LeadingActions>
            <SwipeAction onClick={() => setGastoEditar(gasto)}>
                Editar
            </SwipeAction>
        </LeadingActions>
    )

    const borrarAction = () => (
        <TrailingActions>
            <SwipeAction onClick={() => eliminarGasto(gasto.id)}
                destructive={true}
            >
                Borrar
            </SwipeAction>
        </TrailingActions>
    )

  return (
    <SwipeableList>
        <SwipeableListItem
            leadingActions={editarAction()}
            trailingActions={borrarAction()}
        >
    <div className='gasto sombra'>
      <div className='contenido-gasto'>
        <img 
            src={diccionarioIconos[gasto.categoria]}
        />
        <div className='descripcion-gasto'>
            <p className='categoria'>{gasto.categoria}</p>
            <p className='nombre-gasto'>{gasto.nombre}</p>
            <p className='fecha-gasto'>Agregado el: <span>{generarFecha(gasto.fecha)}</span></p>
            <p className='ayuda-gasto'><span>Desliza hacia la derecha para editar</span></p>
        </div>
      </div>
      <div className='cantidad-gasto'>${gasto.valor}</div>
    </div>
    </SwipeableListItem>
    </SwipeableList>
  )
}

export default Gasto
