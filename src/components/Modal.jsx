import React, { useState, useEffect } from 'react'

import Mensaje from './Mensaje'

import botonCerar from '../img/cerrar.svg'

const Modal = ({setModal, animarModal, setAnimarModal, guardarGasto, gastoEditar, setGastoEditar}) => {

    const [mensaje, setMensaje] = useState('');
    const [nombre, setNombre] = useState('');
    const [valor, setValor] = useState(0);
    const [categoria, setCategoria] = useState('');
    const [id, setId] = useState('');
    const [fecha, setFecha] = useState('');

    useEffect(() => {
      if(Object.keys(gastoEditar).length > 0){
        setNombre(gastoEditar.nombre);
        setValor(gastoEditar.valor);
        setCategoria(gastoEditar.categoria)
        setId(gastoEditar.id)
        setFecha(gastoEditar.fecha)
      }
    }, [])

    const ocultarModal = () => {
        setAnimarModal(false)
        setGastoEditar({})
        setTimeout(() => {
            setModal(false)   
        }, 300);
    }

    const handleSubmit = e => {
      e.preventDefault();

      if( [nombre, valor, categoria].includes('') ){
        setMensaje('Todos los campos son obligatorios');

        setTimeout(() => {
          setMensaje('')
        }, 3000);

        return
      }

      guardarGasto({nombre, valor, categoria, id, fecha});
    }

  return (
    <div className='modal'>
      <div className='cerrar-modal'>
       <img src={botonCerar} alt='cerar modal' onClick={ocultarModal}/>
      </div>

      <form 
      className={`formulario ${animarModal ? "animar" : 'cerrar'}`}
      onSubmit={handleSubmit}
      >
        <legend>{gastoEditar.nombre ? 'Editar gasto' : 'Nuevo gasto'}</legend>
        {mensaje && <Mensaje tipo="error">{mensaje}</Mensaje>}

        <div className='campo'>
          <label htmlFor='nombre'>Nombre gasto</label>
          <input type="text"
            id='nombre' 
            value={nombre}
            onChange={ e => setNombre(e.target.value)}
          />
        </div>

        <div className='campo'>
          <label htmlFor='valor'>Valor</label>
          <input type="number"
            id='valor'
            placeholder='$'
            value={valor}
            onChange={ e => setValor(Number(e.target.value))}
          />
        </div>

        <div className='campo'>
          <label htmlFor='categoria'>Categoria</label>
          <select id="categoria"
            value={categoria}
            onChange={ e => setCategoria(e.target.value)}
          >
            <option value="">-- Selecciona categoria --</option>
            <option value="ahorro">Ahorro</option>
            <option value="comida">Comida</option>
            <option value="casa">Casa</option>
            <option value="varios">Gastos varios</option>
            <option value="ocio">Ocio</option>
            <option value="salud">Salud</option>
            <option value="suscripciones">Suscripciones</option>
          </select>
        </div>

        <input 
          type="submit"
          value={gastoEditar.nombre ? 'Guardar cambios' : 'Agregar gasto'}
        />
      </form>
    </div>
  )
}

export default Modal
